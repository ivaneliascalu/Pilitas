# coding: utf-8
import pilasengine

pilas = pilasengine.iniciar()

#Creando objetos
n=0
cantidad=5
mono = pilas.actores.Mono()

platano=pilas.actores.Banana()*cantidad
mono.aprender("arrastrable")
texto=pilas.actores.Texto()

#creando metodos
def cuando_come(m,p):
    global n
    m.sonreir()
    m.rotacion=[-90,90,0], 0.2
    texto.texto=str(n)
    p.eliminar()#elimina x 1 vez si p=10 -> p=9
    cuando_gana()
    n+=1
    
def cuando_gana():
    global n
    global cantidad
    if n==cantidad-1:
        texto.texto="GANASTE...!"
  
                     
pilas.colisiones.agregar(mono, platano, cuando_come)
pilas.ejecutar()